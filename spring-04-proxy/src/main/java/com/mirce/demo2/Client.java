package com.mirce.demo2;

import com.mirce.demo1.Host;
import com.mirce.demo1.Proxy;

public class Client {

public static void main(String[] args) {
        Host host = new Host();
        ProxyInvocationHandler pih = new ProxyInvocationHandler();
        pih.setRent((Rent)host);
        Rent proxy = (Rent)pih.getProxy();
        proxy.rent();
        }
        }



