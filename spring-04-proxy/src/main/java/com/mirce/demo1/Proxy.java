package com.mirce.demo1;

public class Proxy {
    private Host host;

    public Proxy(Host host) {
        this.host = host;
    }

    public void rent(){
        host.rent();
        this.ShowHouse();
        this.Free();
    }
    public void ShowHouse(){
        System.out.println("参观房子");
    }
    public void Free(){
        System.out.println("交付");
    }
}
