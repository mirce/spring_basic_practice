package com.mirce.demo1;

public class Client {

    public static void main(String[] args) {
        Host host = new Host();
        Proxy h = new Proxy(host);
        h.rent();
    }

}
