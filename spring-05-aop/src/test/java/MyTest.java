import com.mirce.Service.UserImp;
import com.mirce.Service.service;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MyTest {
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("ApplicationContext.xml");
        service user = (service) context.getBean("user");
        user.add();
    }
}
